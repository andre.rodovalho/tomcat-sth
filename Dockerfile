FROM tomcat:9

RUN apt-get update \
	# Add required additional software
	&& apt-get install --no-install-recommends --yes curl nano supervisor openssh-sftp-server openssh-client rsyslog rsync procps cron iputils-ping net-tools \

	# SiteHost standards
	&& mkdir -p /container/config \
	&& rm -rf /etc/supervisor \
	&& rm -rf /etc/rsyslog* \
	&& mkdir -p /etc/supervisor/conf.d \
	&& ln -s /container/config/supervisord.conf /etc/supervisor/supervisord.conf \
	&& rm -rf /usr/local/tomcat/conf && rm -rf /usr/local/tomcat/logs && rm -rf /usr/local/tomcat/webapps \
	&& ln -s /container/config/tomcat/ /usr/local/tomcat/conf \
	&& ln -s /container/logs/tomcat/ /usr/local/tomcat/logs \
	&& ln -s /container/webapps/ /usr/local/tomcat/webapps \

        # Cleanup
        && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY crontab /etc/crontab
RUN chmod 644 /etc/crontab

EXPOSE 8080

ENTRYPOINT ["/usr/bin/supervisord", "-c", "/container/config/supervisord.conf"]
